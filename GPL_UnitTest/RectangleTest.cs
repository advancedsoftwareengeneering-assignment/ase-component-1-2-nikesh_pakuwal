﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;
using Graphical_Programming_Language;

namespace GPL_UnitTest
{
    [TestClass]
    public class RectangleTest
    {
        int textureStyle;
        Brush bb;
        Color colour = Color.Black;

        public RectangleTest()
        {

        }


        private TestContext testContextInstance;
        
        /// <summary>
        /// Gets and sets testContext which provides information and functionality for present test
        /// </summary>
        public TestContext TextContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Unit Test for rectangle
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {
            var rect = new Graphical_Programming_Language.Rectangle();
            int x = 200, y = 200, size1 = 100, size2 = 100;
            rect.set(textureStyle, bb, colour, x, y, size1, size2);
            Assert.AreEqual(200, rect.x);
        }

    }
}
