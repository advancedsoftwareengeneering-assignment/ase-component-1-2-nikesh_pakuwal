﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace GPL_UnitTest
{
    /// <summary>
    /// Summary description for CircleTest
    /// </summary>
    [TestClass]
    public class CircleTest
    {
        int textureStyle;
        Brush bb;
        Color colour = Color.Black;
        public CircleTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion
        /// <summary>
        /// Unit Test for circle
        /// </summary>
        [TestMethod]
        public void CircleMethod()
        {
            var cir = new Graphical_Programming_Language.Circle();
            int x = 200, y = 200, size1 = 100, size2 = 100;
            cir.set(textureStyle, bb, colour, x, y, size1, size2);
            Assert.AreEqual(200, cir.x);
        }
    }
}
