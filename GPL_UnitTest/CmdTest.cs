﻿using Graphical_Programming_Language;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace GPL_UnitTest
{
    /// <summary>
    /// Summary description for CmdTest
    /// </summary>
    [TestClass]
    public class CmdTest
    {
        public CmdTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            string input;
            TextBox textbox = new TextBox();
            input = "counter = 5 \r\n If counter = 5 then \r\n radius + 25 \r\n Circle 5 \r\n EndIf";

            textbox.Text = input;
            Validation validation = new Validation(textbox);

            Boolean expectedOutcome;
            Boolean realOutcome;
            expectedOutcome = true;
            validation.checkLoopAndIfValidation();
            realOutcome = validation.isValidCommand;
            Assert.AreEqual(expectedOutcome, realOutcome);
        }
    }
}
