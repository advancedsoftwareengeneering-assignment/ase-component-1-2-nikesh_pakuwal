﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Graphical_Programming_Language;
using System.Drawing;

namespace GPL_UnitTest
{
    [TestClass]
    public class TriangleTest
    {
        int textureStyle;
        Brush bb;
        Color colour = Color.Black;

        /// <summary>
        /// Unit testing for Triangle
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {
            var tri = new Graphical_Programming_Language.Triangle();
            int xi1 = 100, yi1 = 200, xi2 = 200, yi2 = 200, xii1 = 200, yii1 = 200, xii2 = 200, yii2 = 200, xiii1 = 200, yiii1 = 200, xiii2 = 200, yiii2 = 200;
            tri.set(textureStyle, bb, colour, xi1, yi1, xi2, yi2, xii1, yii1, xii2, yii2, xiii1, yiii1, xiii2, yiii2);
            Assert.AreEqual(100, tri.xi1);

        }
    }
}
