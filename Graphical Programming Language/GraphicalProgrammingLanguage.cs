﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace Graphical_Programming_Language
{
    public partial class GraphicalProgrammingLanguage : Form
    {

        public GraphicalProgrammingLanguage()
        {
            InitializeComponent();
            g = pictureBox.CreateGraphics();
        }

        /// <summary>
        /// Variables to create everyting that is needed
        /// </summary>

        Color btnBorderColor = Color.FromArgb(104, 162, 255);
        Color mainColor = Color.DarkGray;
        Graphics g;
        int size = 2;
        int x, y = -1;
        OpenFileDialog openFile = new OpenFileDialog();
        Boolean moving = false;
        Pen pen;
        String active = "pen";
        String line = "";
        int mouseX, mouseY = 0;
        int loopCounter = 0;
        Boolean hasDrawOrMoveValue = false;
        Validation validate;

        public int radius = 0;
        public int width = 0;
        public int height = 0;
        public int dSize = 0;
        public int counter = 0;

        ShapeFactory shapeFactory = new ShapeFactory();

        Command cmd;

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void checkBoxFill_CheckedChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Run button click to run command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void singleLineCommandBox_TextChanged(object sender, EventArgs e)
        {
            if (singleLineCommandBox.Text.ToLower().Trim() == "run")
            {
                if (multiLineCommandBox != null && multiLineCommandBox.Text != "")
                {
                    validate = new Validation(multiLineCommandBox);

                    if (!validate.isSomethingInvalid)
                    {
                        MessageBox.Show("Everything is working as it has be to...");
                        loadCommand();
                    }
                }
            }
        }

        private void btnMultiLine_Click(object sender, EventArgs e)
        {
            hasDrawOrMoveValue = false;
            if (multiLineCommandBox.Text != null && multiLineCommandBox.Text != "")
            {
                validate = new Validation(multiLineCommandBox);
                if (!validate.isSomethingInvalid)
                {
                    MessageBox.Show("Everything is working as it has be to...");
                    loadCommand();
                }
            }
        }

        /// <summary>
        /// Loads the particular code 
        /// </summary>
        private void loadCommand()
        {
            int numberOfLines = multiLineCommandBox.Lines.Length;

            for (int i = 0; i < numberOfLines; i++)
            {
                String singleLineCommand = multiLineCommandBox.Lines[i];
                singleLineCommand = singleLineCommand.Trim();
                if (!singleLineCommand.Equals(""))
                {
                    Boolean hasDrawto = Regex.IsMatch(singleLineCommand.ToLower(), @"\bdrawto\b");
                    Boolean hasMoveto = Regex.IsMatch(singleLineCommand.ToLower(), @"\bmoveto\b");
                    if (hasDrawto || hasMoveto)
                    {
                        String args = singleLineCommand.Substring(6, (singleLineCommand.Length - 6));
                        String[] parms = args.Split(',');
                        for (int j = 0; j < parms.Length; j++)
                        {
                            parms[j] = parms[j].Trim();
                        }
                        mouseX = int.Parse(parms[0]);
                        mouseY = int.Parse(parms[1]);
                        hasDrawOrMoveValue = true;
                    }
                    else
                    {
                        hasDrawOrMoveValue = false;
                    }
                    if (hasMoveto)
                    {
                        pictureBox.Refresh();
                    }
                }
            }

            for (loopCounter = 0; loopCounter < numberOfLines; loopCounter++)
            {
                string singleLineCommand = multiLineCommandBox.Lines[loopCounter];
                singleLineCommand = singleLineCommand.Trim();
                if (!singleLineCommand.Equals(""))
                {
                    RunCommand(singleLineCommand);
                }
            }
        }

        private void RunCommand(String singleLineCommand)
        {
            Boolean hasPlus = singleLineCommand.Contains('+');
            Boolean hasEquals = singleLineCommand.Contains('=');
            if (hasEquals)
            {
                singleLineCommand = Regex.Replace(singleLineCommand, @"\s+", "");
                string[] words = singleLineCommand.Split(' ');
                //removing spaces between lines
                for (int i = 0; i < words.Length; i++)
                {
                    words[i] = words[i].Trim();
                }
                String firstWord = words[0].ToLower();
                if (firstWord.Equals("if"))
                {
                    Boolean loop = false;
                    if (words[1].ToLower().Equals("radius"))
                    {
                        if (radius == int.Parse(words[3]))
                        {
                            loop = true;
                        }
                    }
                    else if (words[1].ToLower().Equals("width"))
                    {
                        if (width == int.Parse(words[3]))
                        {
                            loop = true;
                        }
                    }
                    else if (words[1].ToLower().Equals("height"))
                    {
                        if (height == int.Parse(words[3]))
                        {
                            loop = true;
                        }
                    }
                    else if (words[1].ToLower().Equals("counter"))
                    {
                        if (counter == int.Parse(words[3]))
                        {
                            loop = true;
                        }
                    }
                    int ifFirstLine = (GetIfFirstLineNumber());
                    int ifLastLine = (GetIfLastLineNumber() - 1);
                    loopCounter = ifLastLine;
                    if (loop)
                    {
                        for (int j = ifFirstLine; j <= ifLastLine; j++)
                        {
                            string singleLineCommand1 = multiLineCommandBox.Lines[j];
                            singleLineCommand1 = singleLineCommand1.Trim();
                            if (!singleLineCommand1.Equals(""))
                            {
                                RunCommand(singleLineCommand1);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("If the condition is False");
                    }
                }
                else
                {
                    string[] words2 = singleLineCommand.Split('=');
                    for (int j = 0; j < words2.Length; j++)
                    {
                        words2[j] = words2[j].Trim();
                    }
                    if (words2[0].ToLower().Equals("radius"))
                    {
                        radius = int.Parse(words2[1]);
                    }
                    else if (words2[0].ToLower().Equals("width"))
                    {
                        width = int.Parse(words2[1]);
                    }
                    else if (words2[0].ToLower().Equals("height"))
                    {
                        height = int.Parse(words2[1]);
                    }
                    else if (words2[0].ToLower().Equals("counter"))
                    {
                        counter = int.Parse(words2[1]);
                    }
                }
            }
            else if (hasPlus)
            {
                singleLineCommand = System.Text.RegularExpressions.Regex.Replace(singleLineCommand, @"\s+", " ");
                string[] words = singleLineCommand.Split(' ');
                if (words[0].ToLower().Equals("repeat"))
                {
                    counter = int.Parse(words[1]);
                    if (words[2].ToLower().Equals("circle"))
                    {
                        int increaseValue = GetSize(singleLineCommand);
                        radius = increaseValue;
                        for (int j = 0; j < counter; j++)
                        {
                            drawCircle(radius);
                            radius += increaseValue;
                        }
                    }
                    else if (words[2].ToLower().Equals("rectangle"))
                    {
                        int increaseValue = GetSize(singleLineCommand);
                        dSize = increaseValue;
                        for (int j = 0; j < counter; j++)
                        {
                            drawRectangle(dSize, dSize);
                            dSize += increaseValue;
                        }
                    }
                    else if (words[2].ToLower().Equals("triangle"))
                    {
                        int increaseValue = GetSize(singleLineCommand);
                        dSize = increaseValue;
                        for (int j = 0; j < counter; j++)
                        {
                            drawTriangle(dSize, dSize, dSize);
                            dSize += increaseValue;
                        }
                    }
                }
                else
                {
                    string[] words2 = singleLineCommand.Split('+');
                    for (int j = 0; j < words2.Length; j++)
                    {
                        words2[j] = words2[j].Trim();
                    }
                    if (words2[0].ToLower().Equals("radius"))
                    {
                        radius += int.Parse(words2[1]);
                    }
                    else if (words2[0].ToLower().Equals("width"))
                    {
                        width += int.Parse(words2[1]);
                    }
                    else if (words2[0].ToLower().Equals("height"))
                    {
                        height += int.Parse(words2[1]);
                    }
                }
            }
            else
            {
                sendDrawCommand(singleLineCommand);
            }
        }

        /// <summary>
        /// Returns size of shapes
        /// </summary>
        /// <param name="lineCommand"></param>
        /// <returns></returns>
        private int GetSize(string lineCommand)
        {
            int value = 0;
            if (lineCommand.ToLower().Contains("radius"))
            {
                int position = (lineCommand.IndexOf("radius") + 6);
                int size = lineCommand.Length;
                String tempLine = lineCommand.Substring(position, (size - position));
                tempLine = tempLine.Trim();
                String newTempLine = tempLine.Substring(1, (tempLine.Length - 1));
                newTempLine = newTempLine.Trim();
                value = int.Parse(newTempLine);
            }
            else if (lineCommand.ToLower().Contains("size"))
            {
                int pos = (lineCommand.IndexOf("size") + 4);
                int size = lineCommand.Length;
                String tempLine = lineCommand.Substring(pos, (size - pos));
                tempLine = tempLine.Trim();
                String newTempLine = tempLine.Substring(1, (tempLine.Length - 1));
                newTempLine = newTempLine.Trim();
                value = int.Parse(newTempLine);
            }
            return value;
        }

        public void commandReader(String enteredCode)
        {
            /*
             * Checks the string written in the String: code and calls appropriate methods accordingly.
             */
            if (enteredCode.Equals("run"))
            {
                // Creates an array of string, retrieves the text from programWindow and stores the lines. 
                string[] multilineCodes = multiLineCommandBox.Lines;

                // Index to count the line number of the text
                int index = 1;

                /*
                 * Iterates to retrive each line of text that was initially written in the programWindow and 
                 * calls commandReader method, of CommandParser class, along with each line of text.
                */
                foreach (String line in multilineCodes)
                {
                    // Increases index every time the line is changed (the loop is iterated).
                    int counter = index++;

                    // Calls commandReader method, of CommandParser class, and sends the line retrieved, counter and syntaxButton's boolean value as the parameter.
                    cmd.commandReader(line, counter);
                }
            }

            else
            {
                // Calls commandReader method, of CommandParser class, and sends the line retrieved, 1 and syntaxButton's boolean value as the parameter.
                cmd.commandReader(enteredCode, 1);
            }
        }


        private void sendDrawCommand(string lineOfCommand)
        {
            String[] shapes = { "circle", "rectangle", "triangle", "polygon" };
            String[] variable = { "radius", "width", "height", "counter", "size" };

            lineOfCommand = System.Text.RegularExpressions.Regex.Replace(lineOfCommand, @"\s +", " ");
            string[] words = lineOfCommand.Split(' ');
            //removing spaces
            for (int i = 0; i < words.Length; i++)
            {
                words[i] = words[i].Trim();
            }
            String startWord = words[0].ToLower();
            Boolean startWordShape = shapes.Contains(startWord);
            if (startWordShape)
            {
                if (startWord.Equals("circle"))
                {
                    Boolean nextWordIsVariable = variable.Contains(words[1].ToLower());
                    if (nextWordIsVariable)
                    {
                        if (words[1].ToLower().Equals("radius"))
                        {
                            drawCircle(radius);
                        }
                    }
                    else
                    {
                        drawCircle(Int32.Parse(words[1]));
                    }
                }
                else if (startWord.Equals("rectangle"))
                {
                    String args = lineOfCommand.Substring(9, (lineOfCommand.Length - 9));
                    String[] parms = args.Split(',');
                    for (int i = 0; i < parms.Length; i++)
                    {
                        parms[i] = parms[i].Trim();
                    }
                    Boolean nextWordIsVariable = variable.Contains(parms[0].ToLower());
                    Boolean anotherWordIsVariable = variable.Contains(parms[1].ToLower());
                    if (nextWordIsVariable)
                    {
                        if (anotherWordIsVariable)
                        {
                            drawRectangle(width, height);
                        }
                        else
                        {
                            drawRectangle(width, Int32.Parse(parms[1]));
                        }
                    }
                    else
                    {
                        if (anotherWordIsVariable)
                        {
                            drawRectangle(Int32.Parse(parms[0]), height);
                        }
                        else
                        {
                            drawRectangle(Int32.Parse(parms[0]), Int32.Parse(parms[1]));
                        }
                    }

                }
                else if (startWord.Equals("triangle"))
                {
                    String args = lineOfCommand.Substring(8, (lineOfCommand.Length - 8));
                    String[] parms = args.Split(',');
                    for (int i = 0; i < parms.Length; i++)
                    {
                        parms[i] = parms[i].Trim();
                    }
                    drawTriangle(Int32.Parse(parms[0]), Int32.Parse(parms[1]), Int32.Parse(parms[2]));
                }
                else if (startWord.Equals("polygon"))
                {
                    String args = lineOfCommand.Substring(8, (lineOfCommand.Length - 8));
                    String[] parms = args.Split(',');
                    for (int i = 0; i < parms.Length; i++)
                    {
                        parms[i] = parms[i].Trim();
                    }
                    if (parms.Length == 8)
                    {
                        drawPolygon(Int32.Parse(parms[0]), Int32.Parse(parms[1]), Int32.Parse(parms[2]), Int32.Parse(parms[3]),
                            Int32.Parse(parms[4]), Int32.Parse(parms[5]), Int32.Parse(parms[6]), Int32.Parse(parms[7]));
                    }
                    else if (parms.Length == 10)
                    {
                        drawPolygon(Int32.Parse(parms[0]), Int32.Parse(parms[1]), Int32.Parse(parms[2]), Int32.Parse(parms[3]),
                            Int32.Parse(parms[4]), Int32.Parse(parms[5]), Int32.Parse(parms[6]), Int32.Parse(parms[7]),
                            Int32.Parse(parms[8]), Int32.Parse(parms[9]));
                    }
                }
            }
            else
            {
                if (startWord.Equals("loop"))
                {
                    counter = int.Parse(words[1]);
                    int loopFirstLine = (GetLoopFirstLineNumber());
                    int loopLastLine = (GetLoopLastLineNumber() - 1);
                    loopCounter = loopLastLine;
                    for (int i = 0; i < counter; i++)
                    {
                        for (int j = loopFirstLine; j <= loopLastLine; j++)
                        {
                            String singleLineCommand = multiLineCommandBox.Lines[j];
                            singleLineCommand = singleLineCommand.Trim();
                            if (!singleLineCommand.Equals(""))
                            {
                                RunCommand(singleLineCommand);
                            }
                        }
                    }
                }
                else if (startWord.Equals("if"))
                {
                    Boolean loop = false;
                    if (words[1].ToLower().Equals("radius"))
                    {
                        if (radius == int.Parse(words[1]))
                        {
                            loop = true;
                        }
                    }
                    else if (words[1].ToLower().Equals("width"))
                    {
                        if (width == int.Parse(words[1]))
                        {
                            loop = true;
                        }
                    }
                    else if (words[1].ToLower().Equals("height"))
                    {
                        if (height == int.Parse(words[1]))
                        {
                            loop = true;
                        }

                    }
                    else if (words[1].ToLower().Equals("counter"))
                    {
                        if (counter == int.Parse(words[1]))
                        {
                            loop = true;
                        }
                    }
                    int ifFirstLine = (GetIfFirstLineNumber());
                    int ifLastLine = (GetIfLastLineNumber() - 1);
                    loopCounter = ifLastLine;
                    if (loop)
                    {
                        for (int j = ifFirstLine; j <= ifLastLine; j++)
                        {
                            String singleLineCommand = multiLineCommandBox.Lines[j];
                            singleLineCommand = singleLineCommand.Trim();
                            if (!singleLineCommand.Equals(""))
                            {
                                RunCommand(singleLineCommand);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// implements loop
        /// </summary>
        /// <returns></returns>
        private int GetIfLastLineNumber()
        {
            int numberOfLines = multiLineCommandBox.Lines.Length;
            int lineNumber = 0;

            for (int i = 0; i < numberOfLines; i++)
            {
                String singleLineCommand = multiLineCommandBox.Lines[i];
                singleLineCommand = singleLineCommand.Trim();
                if (singleLineCommand.ToLower().Equals("endif"))
                {
                    lineNumber = i + 1;
                }
            }
            return lineNumber;
        }

        /// <summary>
        /// implements having if condition
        /// </summary>
        /// <returns></returns>
        private int GetIfFirstLineNumber()
        {
            int numberOfLines = multiLineCommandBox.Lines.Length;
            int lineNumber = 0;

            for (int i = 0; i < numberOfLines; i++)
            {
                String singleLineCommand = multiLineCommandBox.Lines[i];
                singleLineCommand = Regex.Replace(singleLineCommand, @"\s+", " ");
                string[] words = singleLineCommand.Split(' ');
                //removing spaces
                for (int j = 0; j < words.Length; j++)
                {
                    words[j] = words[j].Trim();
                }
                String firstWord = words[0].ToLower();
                singleLineCommand = singleLineCommand.Trim();
                if (firstWord.Equals("if"))
                {
                    lineNumber = i + 1;

                }
            }
            return lineNumber;
        }

        private int GetLoopLastLineNumber()
        {
            try
            {
                int numberOfLines = multiLineCommandBox.Lines.Length;
                int lineNumber = 0;

                for (int i = 0; i < numberOfLines; i++)
                {
                    String singleLineCommand = multiLineCommandBox.Lines[i];
                    singleLineCommand = singleLineCommand.Trim();
                    if (singleLineCommand.ToLower().Equals("endloop"))
                    {
                        lineNumber = i + 1;
                    }
                }
                return lineNumber;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private int GetLoopFirstLineNumber()
        {
            int numberOfLines = multiLineCommandBox.Lines.Length;
            int lineNumber = 0;

            for (int i = 0; i < numberOfLines; i++)
            {
                String singleLineCommand = multiLineCommandBox.Lines[i];
                singleLineCommand = Regex.Replace(singleLineCommand, @"\s+", " ");
                string[] words = singleLineCommand.Split(' ');
                //removing spaces 
                for (int j = 0; j < words.Length; j++)
                {
                    words[j] = words[j].Trim();
                }
                String firstWord = words[0].ToLower();
                singleLineCommand = singleLineCommand.Trim();
                if (firstWord.Equals("loop"))
                {
                    lineNumber = i + 1;
                }
            }
            return lineNumber;
        }

        private void drawPolygon(int p1, int p2, int p3, int p4, int p5, int p6, int p7, int p8)
        {
            Pen myPen = new Pen(mainColor);
            Point[] point = new Point[5];

            point[0].X = mouseX;
            point[0].Y = mouseY;

            point[1].X = mouseX - p1;
            point[1].Y = mouseY - p2;

            point[2].X = mouseX - p3;
            point[2].Y = mouseY - p4;

            point[3].X = mouseX - p5;
            point[3].Y = mouseY - p6;

            point[4].X = mouseX - p7;
            point[4].Y = mouseY - p8;

            g.DrawPolygon(myPen, point);
        }

        private void drawPolygon(int p1, int p2, int p3, int p4, int p5, int p6, int p7, int p8, int p9, int p10)
        {
            Pen myPen = new Pen(mainColor);
            Point[] point = new Point[6];

            point[0].X = mouseX;
            point[0].Y = mouseY;

            point[1].X = mouseX - p1;
            point[1].Y = mouseY - p2;

            point[2].X = mouseX - p3;
            point[2].Y = mouseY - p4;

            point[3].X = mouseX - p5;
            point[3].Y = mouseY - p6;

            point[4].X = mouseX - p7;
            point[4].Y = mouseY - p8;

            point[4].Y = mouseY - p9;
            point[4].Y = mouseY - p10;

            g.DrawPolygon(myPen, point);
        }

        private void drawTriangle(int rBase, int adj, int hyp)
        {
            Pen myPen = new Pen(mainColor);
            Point[] point = new Point[3];

            point[0].X = mouseX;
            point[0].Y = mouseY;

            point[1].X = mouseX - rBase;
            point[1].Y = mouseY;

            point[2].X = mouseX;
            point[2].Y = mouseY - adj;
            g.DrawPolygon(myPen, point);
        }

        private void drawRectangle(int width, int height)
        {
            Pen myPen = new Pen(mainColor);
            g.DrawRectangle(myPen, mouseX - width / 2, mouseY - height / 2, width, height);
        }


        private void drawCircle(int radius)
        {
            Pen myPen = new Pen(mainColor);
            g.DrawEllipse(myPen, mouseX - radius, mouseY - radius, radius * 2, radius * 2);
        }



        /// <summary>
        /// Exits out of the program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        public int _size1, _size2, _size3, _size4, _size5, _size6, _size7, _size8, _size9, _size10, _size11, _size12;
        /// <summary>
        /// Useful for sides of triangle
        /// </summary>
        public int xi1, yi1, xi2, yi2, xii1, yii1, xii2, yii2, xiii1, yiii1, xiii2, yiii2;

        private void GraphicalProgrammingLanguage_Load(object sender, EventArgs e)
        {

        }

        private void multiLineCommandBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            lblStartPosX.Text = (e.X).ToString();
            lblStartPosY.Text = (e.Y).ToString();
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }



        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        Color paintcolor = Color.Brown;
        Brush bb = new HatchBrush(HatchStyle.Cross, Color.PaleVioletRed, Color.FromArgb(255, 128, 255, 255));
        int textureStyle = 5;
        private void button1_Click(object sender, EventArgs e)
        {
            Regex regexDrRectangle = new Regex(@"drawto (.*[\d])([,])(.*[\d]) rectangle (.*[\d])([,])(.*[\d])");
            Regex regexDrCircle = new Regex(@"drawto (.*[\d])([,])(.*[\d]) circle (.*[\d])");
            Regex regexDrTriangle = new Regex(@"drawto (.*[\d])([,])(.*[\d]) triangle (.*[\d])([,])(.*[\d])([,])(.*[\d])");

            Regex regexClear = new Regex(@"clear");
            Regex regexReset = new Regex(@"reset");
            Regex regexMoveto = new Regex(@"moveto (.*[\d])([,])(.*[\d])");

            Regex regexR = new Regex(@"rectangle (.*[\d])([,])(.*[\d])");
            Regex regexC = new Regex(@"circle (.*[\d])");
            Regex regexT = new Regex(@"triangle (.*[\d])([,])(.*[\d])([,])(.*[\d])");



            Match matchDrRectangle = regexDrRectangle.Match(singleLineCommandBox.Text.ToLower());
            Match matchDrCircle = regexDrCircle.Match(singleLineCommandBox.Text.ToLower());
            Match matchDrTriangle = regexDrTriangle.Match(singleLineCommandBox.Text.ToLower());

            Match matchClear = regexClear.Match(singleLineCommandBox.Text.ToLower());
            Match matchReset = regexReset.Match(singleLineCommandBox.Text.ToLower());
            Match matchMT = regexMoveto.Match(singleLineCommandBox.Text.ToLower());

            Match matchR = regexR.Match(singleLineCommandBox.Text.ToLower());
            Match matchC = regexC.Match(singleLineCommandBox.Text.ToLower());
            Match matchT = regexT.Match(singleLineCommandBox.Text.ToLower());


            if (matchDrRectangle.Success || matchDrCircle.Success || matchDrTriangle.Success || matchClear.Success ||
                matchReset.Success || matchMT.Success || matchR.Success || matchC.Success || matchT.Success)
            {
                //--------------------------------------Rectangle with DrawTo---------------------------------//
                if (matchDrRectangle.Success)
                {
                    try
                    {
                        g = pictureBox.CreateGraphics();
                        _size1 = int.Parse(matchDrRectangle.Groups[1].Value);
                        _size2 = int.Parse(matchDrRectangle.Groups[3].Value);
                        _size3 = int.Parse(matchDrRectangle.Groups[4].Value);
                        _size4 = int.Parse(matchDrRectangle.Groups[6].Value);

                        ShapeFactory shapeFactory = new ShapeFactory();
                        Shape c = shapeFactory.GetShape("rectangle");

                        c.set(textureStyle, bb, paintcolor, _size1, _size2, _size3, _size4);
                        c.draw(g);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                //---------------Rectangle-----------------//
                else if (matchR.Success)
                {
                    try
                    {
                        g = pictureBox.CreateGraphics();
                        _size1 = int.Parse(lblStartPosX.Text);
                        _size2 = int.Parse(lblStartPosY.Text);
                        _size3 = int.Parse(matchR.Groups[1].Value);
                        _size4 = int.Parse(matchR.Groups[3].Value);

                        ShapeFactory shapeFactory = new ShapeFactory();
                        Shape c = shapeFactory.GetShape("rectangle");

                        c.set(textureStyle, bb, paintcolor, _size1, _size2, _size3, _size4);
                        c.draw(g);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("wrong parameter: should be \"rectangle width, height\"");
                    }
                }


                //---------------Circle-----------------//
                else if (matchC.Success)
                {
                    try
                    {
                        g = pictureBox.CreateGraphics();
                        _size1 = int.Parse(lblStartPosX.Text);
                        _size2 = int.Parse(lblStartPosY.Text);
                        _size3 = int.Parse(matchC.Groups[1].Value);


                        ShapeFactory shapeFactory = new ShapeFactory();
                        Shape c = shapeFactory.GetShape("circle");

                        c.set(textureStyle, bb, paintcolor, _size1, _size2, _size3 * 2, _size3 * 2);
                        c.draw(g);
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("wrong parameter: should be \"circle radius\"");
                    }
                }



                //--------------------------------------Triangle with DrawTo---------------------------------//
                else if (matchDrTriangle.Success)
                {
                    try
                    {
                        g = pictureBox.CreateGraphics();
                        _size1 = int.Parse(matchDrTriangle.Groups[1].Value);
                        _size2 = int.Parse(matchDrTriangle.Groups[3].Value);

                        _size3 = int.Parse(matchDrTriangle.Groups[4].Value);
                        _size4 = int.Parse(matchDrTriangle.Groups[6].Value);
                        _size5 = int.Parse(matchDrTriangle.Groups[8].Value);


                        xi1 = _size1;
                        yi1 = _size2;
                        xi2 = Math.Abs(_size3);
                        yi2 = _size2;

                        xii1 = _size1;
                        yii1 = _size2;
                        xii2 = _size1;
                        yii2 = Math.Abs(_size4);

                        xiii1 = Math.Abs(_size3);
                        yiii1 = _size2;
                        xiii2 = _size1;
                        yiii2 = Math.Abs(_size4);

                        ShapeFactory shapeFactory = new ShapeFactory();
                        Shape c = shapeFactory.GetShape("triangle");

                        c.set(textureStyle, bb, paintcolor, xi1, yi1, xi2, yi2, xii1, yii1, xii2, yii2, xiii1, yiii1, xiii2, yiii2);
                        c.draw(g);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                //---------------Triangle-----------------//
                else if (matchT.Success)
                {
                    try
                    {
                        g = pictureBox.CreateGraphics();
                        _size1 = int.Parse(lblStartPosX.Text);
                        _size2 = int.Parse(lblStartPosY.Text);

                        _size3 = int.Parse(matchT.Groups[1].Value);
                        _size4 = int.Parse(matchT.Groups[3].Value);
                        _size5 = int.Parse(matchT.Groups[5].Value);


                        xi1 = _size1;
                        yi1 = _size2;
                        xi2 = Math.Abs(_size3);
                        yi2 = _size2;

                        xii1 = _size1;
                        yii1 = _size2;
                        xii2 = _size1;
                        yii2 = Math.Abs(_size4);

                        xiii1 = Math.Abs(_size3);
                        yiii1 = _size2;
                        xiii2 = _size1;
                        yiii2 = Math.Abs(_size4);

                        ShapeFactory shapeFactory = new ShapeFactory();
                        Shape c = shapeFactory.GetShape("triangle");
                        c.set(textureStyle, bb, paintcolor, xi1, yi1, xi2, yi2, xii1, yii1, xii2, yii2, xiii1, yiii1, xiii2, yiii2);
                        c.draw(g);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("wrong parameter: should be \"triangle side, side, side\"");
                    }
                }

                //---------------Clear-----------------//
                else if (matchClear.Success)
                {
                    pictureBox.Refresh();
                    this.pictureBox.BackgroundImage = null;
                }

                //---------------Reset-----------------//
                else if (matchReset.Success)
                {
                    _size1 = 0;
                    _size2 = 0;
                    lblStartPosX.Text = _size1.ToString();
                    lblStartPosY.Text = _size2.ToString();
                }

                //---------------Move To-----------------//
                else if (matchMT.Success)
                {
                    try
                    {
                        _size1 = int.Parse(matchMT.Groups[1].Value);
                        _size2 = int.Parse(matchMT.Groups[3].Value);

                        lblStartPosX.Text = _size1.ToString();
                        lblStartPosY.Text = _size2.ToString();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }

                }
            }
            else
            {
                MessageBox.Show("Command Invalid!");
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Opens sample text files from selected location
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Open file 
            OpenFileDialog openF = new OpenFileDialog();
            openF.Filter = "SampleText(*txt) | *.txt*";
            if (openF.ShowDialog() == DialogResult.OK)
            {
                singleLineCommandBox.Text = File.ReadAllText(openF.FileName);
            }
        }

        /// <summary>
        /// Saves sample text file in any selected location
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Save file 
            SaveFileDialog saveF = new SaveFileDialog();
            saveF.Filter = "SampleText(*txt) | *.txt*";
            if (saveF.ShowDialog() == DialogResult.OK)
            {
                System.IO.File.WriteAllText(saveF.FileName, singleLineCommandBox.Text);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This project is developed by Nikesh Pakuwal \n  Level 6(TopUp)  Student ID: 7000");
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
