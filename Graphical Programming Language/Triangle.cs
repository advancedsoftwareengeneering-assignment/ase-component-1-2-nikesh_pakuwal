﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_Language
{
    /// <summary>
    /// extends shape class
    /// </summary>
    public class Triangle : Shape
    {
        /// <summary>
        /// getting sids of triangle
        /// </summary>
        public int xi1, yi1, xi2, yi2, xii1, yii1, xii2, yii2, xiii1, yiii1, xiii2, yiii2;
        Color colour;
        int textureStyle;
        Brush bb;

        public override void draw(Graphics g)
        {
            Pen p = new Pen(colour, 5);

            g.DrawLine(p, xi1, yi1, xi2, yi2);
            g.DrawLine(p, xii1, yii1, xii2, yii2);
            g.DrawLine(p, xiii1, yiii1, xiii2, yiii2);

        }

        /// <summary>
        /// getting values to draw triangle
        /// </summary>
        /// <param name="textureStyle"></param>
        /// <param name="bb"></param>
        /// <param name="colour"></param>
        /// <param name="list"></param>
        public override void set(int textureStyle, Brush bb, Color colour, params int[] list)
        {
            //_size1, _size2, xi1,yi1,xi2,yi2,xii1,yii1,xii2,yii2,xiii1,yiii1,xiii2,yiii2
            this.textureStyle = textureStyle;
            this.bb = bb;
            this.colour = colour;

            this.xi1 = list[0];
            this.yi1 = list[1];
            this.xi2 = list[2];
            this.yi2 = list[3];

            this.xii1 = list[4];
            this.yii1 = list[5];
            this.xii2 = list[6];
            this.yii2 = list[7];

            this.xiii1 = list[8];
            this.yiii1 = list[9];
            this.xiii2 = list[10];
            this.yiii2 = list[11];
        }
    }
}
