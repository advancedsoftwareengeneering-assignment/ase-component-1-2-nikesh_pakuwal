﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_Language
{
    /// <summary>
    /// Abstract Shape Class
    /// </summary>
    public abstract class Shape : ShapeInterface
    {
        /// <summary>
        /// passing Graphics value
        /// </summary>
        /// <param name="g"></param>
        public abstract void draw(Graphics g);

        /// <summary>
        /// passing value from button click of main form to the shapes
        /// </summary>
        /// <param name="textureStyle"></param>
        /// <param name="bb"></param>
        /// <param name="colour"></param>
        /// <param name="list"></param>
        public abstract void set(int textureStyle, Brush bb, Color colour, params int[] list);
    }
}
