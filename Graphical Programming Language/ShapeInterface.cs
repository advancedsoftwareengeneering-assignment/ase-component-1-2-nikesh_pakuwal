﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_Language
{
    interface ShapeInterface
    {
        void set(int textureStyle, Brush bb, Color colour, params int[] list);
        void draw(Graphics g);
    }
}
