﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graphical_Programming_Language
{
    /// <summary>
    /// extends shape class
    /// </summary>
    public class Circle : Shape
    {
        /// <summary>
        /// getting value in variable for ellipse
        /// </summary>
        public int x, y, size1, size2;
        Color colour;
        int textureStyle;
        Brush bb;

        public override void draw(Graphics g)
        {
            Pen p = new Pen(colour, 5);
            if (textureStyle == 0)
            {
                g.DrawEllipse(p, x, y, size1, size2);
            }
            else
            {
                g.FillEllipse(bb, x, y, size1, size2);
            }
        }

        /// <summary>
        /// getting value from co-ordinates, colour, testureStyle and radius
        /// </summary>
        /// <param name="textureStyle"></param>
        /// <param name="bb"></param>
        /// <param name="c1"></param>
        /// <param name="list"></param>
        public override void set(int textureStyle, Brush bb, Color colour, params int[] list)
        {
            this.textureStyle = textureStyle;
            this.bb = bb;
            this.colour = colour;
            this.x = list[0];
            this.y = list[1];
            this.size1 = list[2];
            this.size2 = list[3];
        }
    }
}
