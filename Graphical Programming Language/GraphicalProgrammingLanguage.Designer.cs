﻿
namespace Graphical_Programming_Language
{
    partial class GraphicalProgrammingLanguage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GraphicalProgrammingLanguage));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.btnSingleLine = new System.Windows.Forms.Button();
            this.labelMulti = new System.Windows.Forms.Label();
            this.labelSingle = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblStartPosY = new System.Windows.Forms.Label();
            this.lblStartPosX = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.multiLineCommandBox = new System.Windows.Forms.TextBox();
            this.btnMultiLine = new System.Windows.Forms.Button();
            this.singleLineCommandBox = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(10, 5, 2, 7);
            this.menuStrip1.Size = new System.Drawing.Size(825, 36);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(45, 24);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(115, 24);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(115, 24);
            this.saveToolStripMenuItem.Text = "Save ";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveAllToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(115, 24);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.infoToolStripMenuItem});
            this.aboutToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(58, 24);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(101, 22);
            this.infoToolStripMenuItem.Text = "Info";
            this.infoToolStripMenuItem.Click += new System.EventHandler(this.infoToolStripMenuItem_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox.Location = new System.Drawing.Point(12, 69);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(407, 460);
            this.pictureBox.TabIndex = 2;
            this.pictureBox.TabStop = false;
            this.pictureBox.Click += new System.EventHandler(this.pictureBox_Click);
            this.pictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseClick);
            // 
            // btnSingleLine
            // 
            this.btnSingleLine.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSingleLine.Image = ((System.Drawing.Image)(resources.GetObject("btnSingleLine.Image")));
            this.btnSingleLine.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSingleLine.Location = new System.Drawing.Point(728, 147);
            this.btnSingleLine.Name = "btnSingleLine";
            this.btnSingleLine.Size = new System.Drawing.Size(76, 32);
            this.btnSingleLine.TabIndex = 3;
            this.btnSingleLine.Text = "      Run";
            this.btnSingleLine.UseVisualStyleBackColor = true;
            this.btnSingleLine.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelMulti
            // 
            this.labelMulti.AutoSize = true;
            this.labelMulti.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMulti.Location = new System.Drawing.Point(443, 264);
            this.labelMulti.Name = "labelMulti";
            this.labelMulti.Size = new System.Drawing.Size(165, 15);
            this.labelMulti.TabIndex = 5;
            this.labelMulti.Text = "Multi-line Command Box";
            // 
            // labelSingle
            // 
            this.labelSingle.AutoSize = true;
            this.labelSingle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSingle.Location = new System.Drawing.Point(443, 164);
            this.labelSingle.Name = "labelSingle";
            this.labelSingle.Size = new System.Drawing.Size(174, 15);
            this.labelSingle.TabIndex = 5;
            this.labelSingle.Text = "Single-line Command Box";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lblStartPosY);
            this.groupBox1.Controls.Add(this.lblStartPosX);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(446, 69);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(162, 72);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Position";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(86, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Y";
            // 
            // lblStartPosY
            // 
            this.lblStartPosY.AutoSize = true;
            this.lblStartPosY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartPosY.Location = new System.Drawing.Point(110, 31);
            this.lblStartPosY.Name = "lblStartPosY";
            this.lblStartPosY.Size = new System.Drawing.Size(29, 16);
            this.lblStartPosY.TabIndex = 0;
            this.lblStartPosY.Text = "000";
            // 
            // lblStartPosX
            // 
            this.lblStartPosX.AutoSize = true;
            this.lblStartPosX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartPosX.Location = new System.Drawing.Point(37, 31);
            this.lblStartPosX.Name = "lblStartPosX";
            this.lblStartPosX.Size = new System.Drawing.Size(29, 16);
            this.lblStartPosX.TabIndex = 0;
            this.lblStartPosX.Text = "000";
            this.lblStartPosX.Click += new System.EventHandler(this.label3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "X";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // multiLineCommandBox
            // 
            this.multiLineCommandBox.BackColor = System.Drawing.SystemColors.MenuText;
            this.multiLineCommandBox.ForeColor = System.Drawing.SystemColors.Window;
            this.multiLineCommandBox.Location = new System.Drawing.Point(443, 282);
            this.multiLineCommandBox.Multiline = true;
            this.multiLineCommandBox.Name = "multiLineCommandBox";
            this.multiLineCommandBox.Size = new System.Drawing.Size(361, 247);
            this.multiLineCommandBox.TabIndex = 8;
            this.multiLineCommandBox.TextChanged += new System.EventHandler(this.multiLineCommandBox_TextChanged);
            // 
            // btnMultiLine
            // 
            this.btnMultiLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMultiLine.Location = new System.Drawing.Point(726, 245);
            this.btnMultiLine.Name = "btnMultiLine";
            this.btnMultiLine.Size = new System.Drawing.Size(78, 31);
            this.btnMultiLine.TabIndex = 9;
            this.btnMultiLine.Text = "Execute";
            this.btnMultiLine.UseVisualStyleBackColor = true;
            this.btnMultiLine.Click += new System.EventHandler(this.btnMultiLine_Click);
            // 
            // singleLineCommandBox
            // 
            this.singleLineCommandBox.BackColor = System.Drawing.SystemColors.MenuText;
            this.singleLineCommandBox.ForeColor = System.Drawing.SystemColors.Window;
            this.singleLineCommandBox.Location = new System.Drawing.Point(446, 183);
            this.singleLineCommandBox.Multiline = true;
            this.singleLineCommandBox.Name = "singleLineCommandBox";
            this.singleLineCommandBox.Size = new System.Drawing.Size(358, 42);
            this.singleLineCommandBox.TabIndex = 10;
            this.singleLineCommandBox.TextChanged += new System.EventHandler(this.singleLineCommandBox_TextChanged);
            // 
            // GraphicalProgrammingLanguage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(825, 541);
            this.Controls.Add(this.singleLineCommandBox);
            this.Controls.Add(this.btnMultiLine);
            this.Controls.Add(this.multiLineCommandBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labelSingle);
            this.Controls.Add(this.labelMulti);
            this.Controls.Add(this.btnSingleLine);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "GraphicalProgrammingLanguage";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.GraphicalProgrammingLanguage_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button btnSingleLine;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.Label labelMulti;
        private System.Windows.Forms.Label labelSingle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblStartPosY;
        private System.Windows.Forms.Label lblStartPosX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox multiLineCommandBox;
        private System.Windows.Forms.Button btnMultiLine;
        private System.Windows.Forms.TextBox singleLineCommandBox;
    }
}

