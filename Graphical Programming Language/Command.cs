﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graphical_Programming_Language
{
	/// <summary>
	/// Class which parses commands or inputs
	/// </summary>
	/// 

	public class Command
	{
		public static IDictionary<String, int> storeVariables = new Dictionary<String, int>();
		public static ArrayList errorList = new ArrayList();

		Validation validate = new Validation();

		/// <summary>
		/// Object of <see cref="ShapeFactory"/>. <br/>
		/// Access the methods and objects of <see cref="ShapeFactory"/> class from <see cref="CommandParser"/> class.
		/// </summary>
		ShapeFactory identifierObject = new ShapeFactory();

		/// <summary>
		/// Stores boolean values: true when method statement is encountered in the program window.
		/// </summary>
		bool methodFlag = false;

		/// <summary>
		/// Object of <see cref="MethodParser"/> class to access method within the class.
		/// </summary>
		Methods methods = new Methods();

		/// <summary>
		/// Stores string value which stores the name of method entered by the user.
		/// </summary>
		String methodName;

		/// <summary>
		/// Method: Triggered when enteredCode has var and expression separated by space and are required to be split.
		/// Splits the var and expression separated by space as different item and saves them into an array of string.
		/// </summary>
		/// <param name="varExpression"> Holds the string value which contains var and expression separated by space</param>
		/// <returns>The string array which contains var and expression separate values as different items</returns>
		public String[] splitVariableExpression(String varExpression)
		{
			// Array of strings which stores code, separated by space, as a different item. 
			String[] splittedVarExp = varExpression.Split(' ');

			return splittedVarExp;
		}

		/// <summary>
		/// Method: Triggered when expression has values separated by '=' and are required to be split.
		/// Splits the parameters separated by '=' as a different item and saves them into an array of string.
		/// </summary>
		/// <param name="expression"> Holds the string value of expression which are separated by '=' </param>
		/// <returns> The string array which contains expression's separated values as different items.</returns>
		public String[] splitExpression(String expression)
		{
			// Array of strings which stores code, separated by '=', as a different item. 
			String[] splittedExpression = expression.Split('=');

			return splittedExpression;
		}

		/// <summary>
		/// Method: Triggered when enteredCode has command and parameters separated by space and are required to be split.
		/// Splits the command and parameters separated by space as different item and saves them into an array of string.
		/// </summary>
		/// <param name="enteredCode"> Holds the string value which contains command and parameters separated by space</param>
		/// <returns>The string array which contains commands and parameters separate values as different items</returns>
		public String[] CommandSplitter(String enteredCode)
		{
			/// Array of strings which stores code, separated by space, as a different item. 
			String[] splittedCommand = enteredCode.Split(' ');

			return splittedCommand;
		}

		/// <summary>
		/// Method: Triggered when parameters has values separated by ',' and are required to be split.
		/// Splits the parameters separated by ',' as a different item and saves them into an array of string.
		/// </summary>
		/// <param name="parameters">Holds the string value of parameters which are separated by ',' </param>
		/// <returns> The string array which contains parameter's separated values as different items.</returns>
		public String[] ParameterSplitter(String parameters)
		{
			// Array of strings which stores parameters, separated by ',' as a different item.
			String[] splittedParameter = parameters.Split(',');

			return splittedParameter;
		}

		/// <summary>
		/// Method Triggered when some text is written in the program Windown and run command is pressed. <br/>
		/// Checks the entered Code line by line for built in commands like 'var', 'if statement', 'while statement' and also performs variable operations like (+), (-), (*) and (/)
		/// </summary>
		/// <param name="enteredCode">Holds each line of command retrieved from the program Window of the application</param>
		/// <param name="lineCounter">Holds the number of line of the text</param>
		/// <param name="syntaxButton">Holds the boolean value of syntaxButton which confirms if syntaxButton was pressed in the application</param>
		public void commandReader(String enteredCode, int lineCounter)
		{

			// Splits the enteredCode with '(' and stores in tempName
			String tempName = enteredCode.Split('(')[0];

			// Stores the first word of the enteredCode of every line in the Program Window to check for built in commands like if, method or while.
			String declareName = enteredCode.Split(' ')[0];

			// Checks if the keywoord is method and performs task underneath.
			if (declareName.Equals("method"))
			{
				// calls a method of performMethod class which checks and splits method statements and returns true when method is found and false otherwise.
				methodFlag = methods.identifyMethod(enteredCode, lineCounter);
				// Retrieves the name of method and stores in the string variable.
				methodName = methods.methodName;
			}
			// Checks if the methodFlag
			else if (methodFlag)
			{
				// calls a method of performMethod class which checks and returns true if the endmethod statement is encountered and false otherwise.
				methodFlag = methods.storeMethodCommands(enteredCode);
			}
			// Checks if the entered code is methodname as method is called if so and performs the task underneath.
			else if (enteredCode.Equals(methodName + "()"))
			{
				//Iterates to retrieve each line after method statement and before endmethod
				foreach (String eachLineCode in methods.methodCommands)
				{
					// Calls commandReader to execute each line retrieved from the arraylIst of codes.
					commandReader(eachLineCode, lineCounter);
					validate.isValidCommand = true;
				}
			}
			// Checks if the entered code is methodName with parameters and and performs task underneath
			else if (tempName.Equals(methodName))
			{
				validate.isValidCommand = true;

				try
				{
					// splits the enteredcode with '(' and stores in string array.
					String[] splittedCallMethod = enteredCode.Split('(');

					// Splits the enteredcode with respect to '(' and ')' and stores second value in methodParameters
					String methodParameters = enteredCode.Split('(', ')')[1];
					// Splits all parameters with comma and stores in string array.
					String[] parameters = methodParameters.Split(',');

					// Checks if the mehtod name matches with entered methodname.
					if (splittedCallMethod[0].Equals(methodName))
					{
						// Checks if the parameters lenght are equal
						if (parameters.Length == methods.splittedParameters.Length)
						{
							// Iterates to take each parameter and performs task underneath
							for (int index = 0; index < parameters.Length; index++)
							{
								// Matches and assigns values of parameters to variable declared while declaring method
								storeVariables[methods.splittedParameters[index]] = Convert.ToInt32(parameters[index]);
								validate.isValidCommand = true;
								// Checks if the stored variable dictionary contains the entered variable as key.
								if (storeVariables.ContainsKey(methods.splittedParameters[index]))
								{
									// checks the value of the variable stored in dictionary.
									methods.splittedParameters[index] = storeVariables[methods.splittedParameters[index]].ToString();
									validate.isValidCommand = true;
								}
							}

							// Iterates to retrieve each line of code under method and above endmethod and also runs.
							foreach (String eachLineCode in methods.methodCommands)
							{
								// Calls the commandReader method to execute all command inside the arraylist.
								commandReader(eachLineCode, lineCounter);
								validate.isValidCommand = true;
							}
						}
						else
						{
							// Throws user defiended exception.
							throw new InvalidCommand("Invalid Method Command");
						}
					}
				}
				// catchees
				catch (IndexOutOfRangeException)
				{
					// Adds the following line as error in the errorList
					errorList.Add("ERROR!!! AT LINE " + lineCounter + ". Please Enter Correct Method statement");
				}
				catch (InvalidCommand)
				{
					// Adds the following line as error in the errorList
					errorList.Add("ERROR!!! AT LINE " + lineCounter + ". Please Enter Correct Method statement");
				}

			}

			// Checks if the specific line of enteredCode contains '+' sign and performs tasks underneath.
			else if (enteredCode.Contains("+"))
			{
				// Splits the statement with respect to the '+' sign and stores separated values in the string array
				String[] expParameter = enteredCode.Split('+');

				// Checks if the first parameter in the string array is a variable stored in data dictionary and performs the tasks underneath.
				if (storeVariables.ContainsKey(expParameter[0]))
				{
					// Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
					if (storeVariables.ContainsKey(expParameter[1]))
					{
						// Adds the values entered along with the operator and hence changes the value stored in that particular variable.
						storeVariables[expParameter[0]] = storeVariables[expParameter[0]] + storeVariables[expParameter[1]];
					}
					else
					{
						// Adds the values entered along with the operator and hence changes the value stored in that particular variable.
						storeVariables[expParameter[0]] = storeVariables[expParameter[0]] + Convert.ToInt32(expParameter[1]);
					}
				}
				else
				{
					// Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
					if (storeVariables.ContainsKey(expParameter[1]))
					{
						// Adds the values entered along with the operator and hence changes the value stored in that particular variable.
						storeVariables[expParameter[1]] = storeVariables[expParameter[1]] + Convert.ToInt32(expParameter[0]);
					}
				}
			}
			// Checks if the specific line of enteredCode contains '-' sign and performs tasks underneath.
			else if (enteredCode.Contains("-"))
			{
				// Splits the statement with respect to the '-' sign and stores separated values in the string array
				String[] expParameter = enteredCode.Split('-');

				//Checks if the first parameter in the string array is a variable stored in data dictionary and performs the tasks underneath.
				if (storeVariables.ContainsKey(expParameter[0]))
				{
					// Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
					if (storeVariables.ContainsKey(expParameter[1]))
					{
						// Adds the values entered along with the operator and hence changes the value stored in that particular variable.
						storeVariables[expParameter[0]] = storeVariables[expParameter[0]] - storeVariables[expParameter[1]];
					}
					else
					{
						// Adds the values entered along with the operator and hence changes the value stored in that particular variable.
						storeVariables[expParameter[0]] = storeVariables[expParameter[0]] - Convert.ToInt32(expParameter[1]);
					}
				}
				else
				{
					// Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
					if (storeVariables.ContainsKey(expParameter[1]))
					{
						// Adds the values entered along with the operator and hence changes the value stored in that particular variable.
						storeVariables[expParameter[1]] = storeVariables[expParameter[1]] - Convert.ToInt32(expParameter[0]);
					}
				}
			}
			// Checks if the specific line of enteredCode contains '*' sign and performs tasks underneath.
			else if (enteredCode.Contains("*"))
			{
				// Splits the statement with respect to the '*' sign and stores separated values in the string array
				String[] expParameter = enteredCode.Split('*');

				// Checks if the first parameter in the string array is a variable stored in data dictionary and performs the tasks underneath.
				if (storeVariables.ContainsKey(expParameter[0]))
				{
					// Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
					if (storeVariables.ContainsKey(expParameter[1]))
					{
						// Adds the values entered along with the operator and hence changes the value stored in that particular variable.
						storeVariables[expParameter[0]] = storeVariables[expParameter[0]] * storeVariables[expParameter[1]];
					}
					else
					{
						// Adds the values entered along with the operator and hence changes the value stored in that particular variable.
						storeVariables[expParameter[0]] = storeVariables[expParameter[0]] * Convert.ToInt32(expParameter[1]);
					}
				}
				else
				{
					// Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
					if (storeVariables.ContainsKey(expParameter[1]))
					{
						// Adds the values entered along with the operator and hence changes the value stored in that particular variable.
						storeVariables[expParameter[1]] = storeVariables[expParameter[1]] * Convert.ToInt32(expParameter[0]);
					}
				}
			}
			// Checks if the specific line of enteredCode contains '/' sign and performs tasks underneath.
			else if (enteredCode.Contains("/"))
			{
				// Splits the statement with respect to the '/' sign and stores separated values in the string array
				String[] expParameter = enteredCode.Split('/');

				// Checks if the first parameter in the string array is a variable stored in data dictionary and performs the tasks underneath.
				if (storeVariables.ContainsKey(expParameter[0]))
				{
					// Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
					if (storeVariables.ContainsKey(expParameter[1]))
					{
						// Adds the values entered along with the operator and hence changes the value stored in that particular variable.
						storeVariables[expParameter[0]] = storeVariables[expParameter[0]] / storeVariables[expParameter[1]];
					}
					else
					{
						// Adds the values entered along with the operator and hence changes the value stored in that particular variable.
						storeVariables[expParameter[0]] = storeVariables[expParameter[0]] / Convert.ToInt32(expParameter[1]);
					}
				}
				else
				{
					// Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
					if (storeVariables.ContainsKey(expParameter[1]))
					{
						// Adds the values entered along with the operator and hence changes the value stored in that particular variable.
						storeVariables[expParameter[1]] = storeVariables[expParameter[1]] / Convert.ToInt32(expParameter[0]);
					}
				}
			}

		}
	}
}
