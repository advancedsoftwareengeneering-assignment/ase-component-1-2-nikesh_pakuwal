﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_Language
{
    /// <summary>
    /// extends shape class
    /// </summary>
    public class Rectangle : Shape
    {
        /// <summary>
        /// getting value variable for rectangle
        /// </summary>
        public int x, y, size1, size2;
        public int textureStyle;

        public Brush bb;
        public Color colour;

        /// <summary>
        /// Draw rectangle
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g)
        {
            Pen p = new Pen(colour, 5);
            if(textureStyle == 0)
            {
                g.DrawRectangle(p, x, y, size1, size2);
            }
            else
            {
                g.FillRectangle(bb, x, y, size1, size2);
            }
        }

        /// <summary>
        /// getting values to draaw rectangle
        /// </summary>
        /// <param name="textureStyle"></param>
        /// <param name="bb"></param>
        /// <param name="colour"></param>
        /// <param name="list"></param>
        public override void set(int textureStyle, Brush bb, Color colour, params int[] list)
        {
            this.textureStyle = textureStyle;
            this.bb = bb;
            this.colour = colour;
            this.x = list[0];
            this.y = list[1];
            this.size1 = list[2];
            this.size2 = list[3];
        }
    }
}
